#!/usr/bin/env node
// Author Leonardo F. Bauchwitz
// Febrar 05 2020 15 hs.

// Setup data

// FBI Database for most wanted  criminals saved in a map, where the key is actual name, and value their alias.
const  criminals = new Map();
criminals.set("Paul White", "Roger Night, Peter Llong Jr.");
criminals.set("Roger Fedexer", "Rob Ford, Pete Lord, Roger McWire");
criminals.set("Paul White Jr.", null);
criminals.set("Red Fortress", "Roger Rabbit, Ross Winter");
criminals.set("Redfort Fort", "Red Strong, Red Fort");
//criminals.set("Juan Perez", "Paco, El tiburón, Sarmientito");
//criminals.set("Red Jarod", "Pazero, Roger Wind");

const located = new Map(); // To save the search results

// Function

function getByValue(map, searchValue) {
    // Searching in key -- More weight 
    for (let [key, value] of map.entries()) {
	var n = -1;
	//	if (key.toUpperCase() == searchValue) return 'First name: ' + key  + ' Aliases: '+ value;  // Assumption: The key is uniqueSame - One result
	if (key.toUpperCase() == searchValue) {
	    located.set(key,value); // Add to map located
	    return located;
	}
	n = key.toUpperCase().search(searchValue); // Maybe more than one result.
//	console.log('Search in name n: '+ n);
	if (n != -1)
	    located.set(key,value); //Add to map located
	    //return 'First name: ' + key  + ' Aliases: '+ value;
    }
//    console.log(located.size);
    if (located.size != 0) return located;

    // Searching in value -- less weight
    for (let [key, value] of map.entries()) {
	var n = -1;
	if (value != null){ // Problem with null, because cannot read property 	    
	    n = value.toUpperCase().search(searchValue); 
//	    console.log('Busqueda en alias n:'+ n);	    
	};
	if(n != -1) located.set(key,value);
    }
     if (located.size != 0) return located;
    return false;
}


// Call the program with arguments like  $ ./index.js roger
// Input argument is name (true name or alias name for criminal)
process.argv.splice(0, 2);
var name = process.argv.join(" ");
name = name.toUpperCase();


if (!getByValue(criminals,name))
    console.log('Not Match')
else{

for (let [key,value] of located.entries())
    console.log('First name: '+ key + ' Aliases: '+ value);
}


// Ask for the search string

// const readline = require("readline");
// const rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
// });
// var name;
// rl.question(`Enter the search string, add a space to the end, if you wish search only that word:`, (name) => {
//     name = name.toUpperCase();
//     console.log(`Search string: ${name}`)
//     getByValue(criminals,name);

// for (let [key,value] of located.entries())
//     console.log('First name: '+ key + ' Aliases: '+ value);
    
//   rl.close()
// })


